FROM ubuntu:18.04 as builder
ENV DEBIAN_FRONTEND=noninteractive 

RUN apt-get update
RUN apt-get install -y \
    lsb-core \
    build-essential \
    cmake \
    software-properties-common \
    wget    

RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list' && \
    apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
RUN apt-get update
RUN apt-get install -y \
    ros-melodic-ros-base \
    python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential

WORKDIR /
RUN /bin/bash -c "source /opt/ros/melodic/setup.bash"

RUN rosdep init && \
    rosdep update

RUN apt-get update && \
    apt-get install -y \
    ros-melodic-tf2-geometry-msgs \
    python-catkin-tools \
    ros-melodic-rqt-multiplot \
    ros-melodic-joy \
    ros-melodic-cv-bridge \
    ros-melodic-image-transport \
    libyaml-cpp-dev \
    libcurl4-openssl-dev

WORKDIR /root

RUN git clone https://github.com/FS-Online/Formula-Student-Driverless-Simulator.git --recurse-submodules
RUN cd Formula-Student-Driverless-Simulator/AirSim && \
    ./setup.sh && ./build.sh